import { ipcRenderer } from "electron";
import { NewsConfig } from "./configs/configs";

export interface NewsTitle{
    title:string,
    secondaryText?: string,
    url:string,
    imageUrl: string | null,

}


export async function getTitles (configList:NewsConfig[]):Promise<NewsTitle[]>{
    const titles: NewsTitle[] = await new Promise((resolve) => {

        ipcRenderer.once('getTitles', (event, titles:NewsTitle[]) => {
            resolve(titles);
        })
        ipcRenderer.send('getTitles', configList);
    });

    return titles;
}
