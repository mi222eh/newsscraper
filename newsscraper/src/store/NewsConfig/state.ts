import { NewsConfig } from "../../app/scraper/configs/configs";
export interface NewsConfigState{
  configList: NewsConfig[]
}

export default function ():NewsConfigState {
  return {
    configList:[]
  }
}
